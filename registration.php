<?php
use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE, 'Swissclinic_WeltPixelGTMExtension', __DIR__
);
